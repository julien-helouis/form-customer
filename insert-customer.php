<?php
    session_start();

    if(isset($_GET['lang'])) {
        $transFile = 'trans/' . $_GET['lang'] . '.php';
        if(!file_exists ($transFile)){
            $transFile = 'trans/fr.php';
        }

    } else {
        $transFile = 'trans/fr.php';
    }
    include_once $transFile;

    $class = '';
    $alert = '';

    if(isset($_POST['captcha_txt'])){
        if( md5($_POST['captcha_txt']) == $_SESSION['captcha'] ) {
            $post = $_POST;
            $post = http_build_query($post);

            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                        . "Content-Length: " . strlen($post) . "\r\n"
                        . "Authorization: Basic Ymlic2VydmU6Ymlic2VydmU9MSA=\r\n",
                    'content' => $post
                )
            );

            $context = stream_context_create($context_options);
            $fp = fopen('http://localhost/extended/web/app_dev.php/customers', 'r', false, $context);

            if ($fp === false) {
                $alert = "error";
            } else {
                $alert = "ok";
            }
        } else {
            $class = 'error';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $trans['h1']; ?></title>
    <link href="css/stylesheet.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/user.js"></script>
</head>
<body>
    <header>
        <menu>
            <span><?php echo $trans['change_style']; ?></span>
        </menu>
        <section>
            <div class="logos">
                <img src="img/wyz.jpg" title="WYZ" alt="WYZ" width="250" height="88" /> pour <img src="img/michelin.jpg" title="Michelin" alt="Michelin" width="250" height="76" />
            </div>
        </section>
    </header>
    <main>
        <section>
            <h1><?php echo $trans['h1']; ?></h1>
            <?php
                if ($alert == "error") {
            ?>
                    <div class="alert error"><p><?php echo $trans['insert_error']; ?></p></div>
            <?php
                } elseif ($alert == "ok") {
            ?>
                    <div class="alert success"><p><?php echo $trans['insert_success']; ?></p></div>
            <?php
                }
            ?>
            <form method="post" id="insert_customer_form" enctype="multipart/form-data">
                <div class="ligne">
                    <label><?php echo $trans['user_id']; ?></label>
                    <input type="text" name="user_id" id="user_id" title="user_id">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['customer_group']; ?></label>
                    <input type="text" name="customer_group" id="customer_group" title="customer_group">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['lang']; ?></label>
                    <select name="language" id="language" title="language">
                        <option value="">--</option>
                        <option value="1">Français</option>
                        <option value="2">Anglais</option>
                        <option value="3">Espagnol</option>
                        <option value="4">Portuguais</option>
                        <option value="5">Suédois</option>
                    </select>
                </div>
                <div class="ligne">
                    <label><?php echo $trans['buyer_party']; ?></label>
                    <input type="text" name="buyer_id" id="buyer_id" title="buyer_id">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['ordering_party']; ?></label>
                    <input type="text" name="ordering_id" id="ordering_id" title="ordering_id">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['consignee']; ?></label>
                    <input type="text" name="consignee_id" id="consignee_id" title="consignee_id">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['username']; ?></label>
                    <input type="text" name="username" id="username" title="username">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['email']; ?></label>
                    <input type="text" name="email" id="email" title="email">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['password']; ?></label>
                    <input type="password" name="password" id="password" title="password">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['company']; ?></label>
                    <input type="text" name="company" id="company" title="company">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['brand']; ?></label>
                    <input type="text" name="brand" id="brand" title="brand">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['referent_title']; ?></label>
                    <select name="title_id" id="title_id" title="title_id">
                        <option value="">--</option>
                        <option value="1">Madame</option>
                        <option value="2">Monsieur</option>
                    </select>
                </div>
                <div class="ligne">
                    <label><?php echo $trans['firstname']; ?></label>
                    <input type="text" name="firstname" id="firstname" title="firstname">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['lastname']; ?></label>
                    <input type="text" name="lastname" id="lastname" title="lastname">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['address1']; ?></label>
                    <input type="text" name="address1" id="address1" title="address1">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['address2']; ?></label>
                    <input type="text" name="address2" id="address2" title="address2">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['zipcode']; ?></label>
                    <input type="text" name="zipcode" id="zipcode" title="zipcode">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['city']; ?></label>
                    <input type="text" name="city" id="city" title="city">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['area']; ?></label>
                    <select name="area_id" title="area_id" id="area_id">
                        <option value="">--</option>
                        <option value="60">60 - Oise</option>
                        <option value="75">75 - Paris</option>
                        <option value="80">80 - Somme</option>
                    </select>
                </div>
                <div class="ligne">
                    <label><?php echo $trans['country']; ?></label>
                    <select name="country_id" title="country_id" id="country_id">
                        <option value="">--</option>
                        <option value="1">France</option>
                        <option value="2">Belgique</option>
                        <option value="3">Espagne</option>
                    </select>
                </div>
                <div class="ligne">
                    <label><?php echo $trans['delivery_address1']; ?></label>
                    <input type="text" name="delivery_address1" id="delivery_address1" title="delivery_address1">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['delivery_address2']; ?></label>
                    <input type="text" name="delivery_address2" id="delivery_address2" title="delivery_address2">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['delivery_zipcode']; ?></label>
                    <input type="text" name="delivery_zipcode" id="delivery_zipcode" title="delivery_zipcode">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['delivery_city']; ?></label>
                    <input type="text" name="delivery_city" id="delivery_city" title="delivery_city">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['siret_number']; ?></label>
                    <input type="text" name="siret_number" id="siret_number" title="siret_number">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['vat_number']; ?></label>
                    <input type="text" name="vat_number" id="vat_number" title="vat_number">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['phone']; ?></label>
                    <input type="text" name="phone" id="phone" title="phone">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['mobile']; ?></label>
                    <input type="text" name="mobile" id="mobile" title="mobile">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['fax']; ?></label>
                    <input type="text" name="fax" id="fax" title="fax">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank']; ?></label>
                    <input type="text" name="bank" id="bank" title="bank">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_phone']; ?></label>
                    <input type="text" name="bank_phone" id="bank_phone" title="bank_phone">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_code']; ?></label>
                    <input type="text" name="bank_code" id="bank_code" title="bank_code">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_counter']; ?></label>
                    <input type="text" name="bank_counter" id="bank_counter" title="bank_counter">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_account']; ?></label>
                    <input type="text" name="bank_account" id="bank_account" title="bank_account">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_rib']; ?></label>
                    <input type="text" name="bank_rib" id="bank_rib" title="bank_rib">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_swift']; ?></label>
                    <input type="text" name="bank_swift" id="bank_swift" title="bank_swift">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['bank_iban']; ?></label>
                    <input type="text" name="bank_iban" id="bank_iban" title="bank_iban">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['prepaid_limit']; ?></label>
                    <input type="text" name="prepaid_limit" id="prepaid_limit" title="prepaid_limit">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['max_quantity']; ?></label>
                    <input type="text" name="max_quantity" id="max_quantity" title="max_quantity">
                </div>
                <div class="ligne">
                    <label><?php echo $trans['attachment']; ?></label>
                    <input type="file" name="attachment" id="attachment" title="attachment">
                </div>
                <div class="ligne special">
                    <input type="checkbox" name="newsletter" id="newsletter" title="newsletter">
                    <span><?php echo $trans['newsletter']; ?></span>
                </div>
                <div class="ligne">
                    <label>
                        <?php echo $trans['captcha_txt']; ?>
                        <img src="captcha.php" alt="Captcha" title="Captcha" id="captcha">
                    </label>
                    <input type="text" name="captcha_txt" id="captcha_text" title="captcha_txt" <?php echo $class; ?>>
                </div>
                <div class="button">
                    <span class="send_button"><?php echo $trans['save_button']; ?></span>
                </div>
            </form>
        </section>
    </main>
    <footer>
        <p>&copy; WYZ Group 2017</p>
    </footer>
</body>
</html>