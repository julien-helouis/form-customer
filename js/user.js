$(function() {
    $('.button span').on('click', function(){
        var $check = true;

        $('input[type=text]').each(function(){
            if($(this).val() === ''){
                $(this).addClass('error');
                $check = false;
            }
        });

        $('select').each(function(){
            if($(this).val() === ''){
                $(this).addClass('error');
                $check = false;
            }
        });

        if($check === true){
            $('#insert_customer_form').submit();
        }
    });

    $('menu span').on('click', function(){
        if($('link').attr('href') === 'css/stylesheet.css'){
            $('link').attr("href", "css/stylesheetv2.css");
        } else {
            $('link').attr("href", "css/stylesheet.css");
        }

    });
});